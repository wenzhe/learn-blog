package online.zhangwenzhe.common.security;


public interface TokenVerifier {
    /**
     * token验证
     * @param token
     * @return
     */
    String verify(String token);
}
