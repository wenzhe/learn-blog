package online.zhangwenzhe.common.security.datasource;

import java.util.ArrayList;
import java.util.List;

public class ResourceRoleMapVo {
    private String uri;
    private String method;
    private List<String> roleIdList;

    public ResourceRoleMapVo() {
        roleIdList = new ArrayList<>();
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<String> getRoleIdList() {
        return roleIdList;
    }

    public void setRoleIdList(List<String> roleIdList) {
        this.roleIdList = roleIdList;
    }
}
