package online.zhangwenzhe.common.security.Impl;

import online.zhangwenzhe.common.security.Authenticator;
import online.zhangwenzhe.common.security.AuthorityVerifier;
import online.zhangwenzhe.common.security.TokenVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class AuthenticatorImpl implements Authenticator {

    @Autowired
    TokenVerifier tokenVerifier;

    @Autowired
    AuthorityVerifier authorityVerifier;


    @Override
    public String verifyToken(String token) {
        if (StringUtils.isEmpty(token)) {
            return null;
        }

        String username = tokenVerifier.verify(token);
        return username;
    }

    @Override
    public boolean verifyAuthority(String username, String uri, String httpMethod) {
        if (StringUtils.isEmpty(uri) || StringUtils.isEmpty(httpMethod)) {
            return false;
        }

        return authorityVerifier.verify(username, uri, httpMethod);
    }
}
