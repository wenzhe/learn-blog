package online.zhangwenzhe.common.security;


public interface AuthorityVerifier {
    /**
     * 权限验证
     * @param username
     * @param uri
     * @param httpMethod
     * @return
     */
    boolean verify(String username, String uri, String httpMethod);
}
