package online.zhangwenzhe.common.security;

import org.springframework.stereotype.Component;

@Component
public interface Authenticator {
    /**
     * Token验证
     * @param token
     * @return 验证成功返回用户标识(username)
     */
    String verifyToken(String token);

    /**
     * 权限验证
     * @param username
     * @param uri
     * @param httpMethod
     * @return
     */
    boolean verifyAuthority(String username, String uri, String httpMethod);
}
