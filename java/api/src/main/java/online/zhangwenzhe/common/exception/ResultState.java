package online.zhangwenzhe.common.exception;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ResultState {

    OK(200,"成功"),
    PARAMETER_INVALID(410,"参数校验出错"),
    BUSINESS_ERROR(420,"业务处理异常");

    @JsonValue
    private int stateCode;
    private String description;
    private ResultState(int stateCode, String description){
        this.stateCode = stateCode;
        this.description = description;
    }

    @Override
    public String toString() {
        return Integer.toString(this.stateCode);
    }
}
