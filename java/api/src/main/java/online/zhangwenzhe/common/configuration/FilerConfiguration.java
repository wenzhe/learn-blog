package online.zhangwenzhe.common.configuration;

import online.zhangwenzhe.common.requestInfo.SequenceIncrementFilter;
import online.zhangwenzhe.common.httpLog.LoggingFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilerConfiguration {
    @Bean
    public FilterRegistrationBean loggingFilterRegistration() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new LoggingFilter());
        registration.addUrlPatterns("/*");
        registration.addInitParameter("paramName", "paramValue");
        registration.setName("LoggingFilter");
        registration.setOrder(2);
        return registration;
    }

    @Bean
    public FilterRegistrationBean requestSequenceFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new SequenceIncrementFilter());
        registration.addUrlPatterns("/*");
        registration.addInitParameter("paramName", "paramValue");
        registration.setName("RequestSequenceFilter");
        registration.setOrder(1);
        return registration;
    }
}
