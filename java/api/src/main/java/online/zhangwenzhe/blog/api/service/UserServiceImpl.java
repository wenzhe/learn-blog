package online.zhangwenzhe.blog.api.service;

import online.zhangwenzhe.blog.api.entity.BlogUserBasic;
import online.zhangwenzhe.blog.api.entity.BlogUserBasicExample;
import online.zhangwenzhe.blog.api.mapper.BlogUserBasicMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private BlogUserBasicMapper blogUserBasicMapper;

    @Override
    public List<BlogUserBasic> list(BlogUserBasicExample blogUserBasicExample){
        blogUserBasicExample = new BlogUserBasicExample();
        return blogUserBasicMapper.selectByExample(blogUserBasicExample);
    }
    public BlogUserBasic get(long id){
        return blogUserBasicMapper.selectByPrimaryKey(id);
    }

    @Override
    public BlogUserBasic getByName(String name){
        //name = "张文哲";
        BlogUserBasicExample blogUserBasicExample = new BlogUserBasicExample();
        blogUserBasicExample.createCriteria().andNicknameEqualTo(name);

        BlogUserBasic userBasic = blogUserBasicMapper.selectByExample(blogUserBasicExample)
                .stream()
                .findFirst()
                .orElse(null);
        return userBasic;
    }

    public int insert(BlogUserBasic user){
        return blogUserBasicMapper.insert(user);
    }

    public int delete(long id){
        return blogUserBasicMapper.deleteByPrimaryKey(id);
    }
}
