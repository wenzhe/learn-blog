package online.zhangwenzhe.blog.api.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import online.zhangwenzhe.common.validation.extend.IDCardNumber;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class UserVo {

    @Min(value = 1, message = "{user.name.notBlank}")
    private long id;

    private String username;

    private String cellphone;

    private String email;

    @NotNull(message = "{user.password.notBlank}")
    private String password;

    private String nickname;

    @IDCardNumber(message = "")
    private String idCard;

    @JsonProperty("is_deleted")
    private Byte isDeleted;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;
}
