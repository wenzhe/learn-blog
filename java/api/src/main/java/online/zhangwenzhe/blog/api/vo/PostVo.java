package online.zhangwenzhe.blog.api.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class PostVo {

    private Long id;

    private String title;

    private String content;

    private Long categoryId;

    private Long userId;

    private Byte isDeleted;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;

    private List<CommentVo> comments;
}
