package online.zhangwenzhe.blog.api.web;

import online.zhangwenzhe.blog.api.entity.BlogUserBasic;
import online.zhangwenzhe.blog.api.service.UserServiceImpl;
import online.zhangwenzhe.blog.api.vo.UserVo;
import online.zhangwenzhe.common.exception.BusinessException;
import online.zhangwenzhe.common.exception.BusinessExceptionResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @RequestMapping(value = "/list")
    public List<BlogUserBasic> list() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return userService.list(null);
    }

    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public UserVo post(@RequestBody UserVo userVo,
                       @NotNull(message = "test不可以为空") String test) {
        return userVo;
    }

    @RequestMapping(path = "/update", method = RequestMethod.PUT)
    public String put() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        int a = 0;
        int b = 1;
        int c = b / a;
        return "post";
    }

    @RequestMapping(path = "/user", method = RequestMethod.DELETE)
    public String delete() {
        BusinessExceptionResult result = new BusinessExceptionResult("ASD001","业务数据发生异常");
        throw new BusinessException(result);
    }


}
