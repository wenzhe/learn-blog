package online.zhangwenzhe.blog.api.vo;

import lombok.Data;

import java.util.Date;

@Data
public class CommentVo {

    private Long id;

    private String title;

    private String content;

    private Long postId;

    private Long userId;

    private Byte isDeleted;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;
}
