package online.zhangwenzhe.blog.api.mapper;

import java.util.List;
import online.zhangwenzhe.blog.api.entity.BlogComment;
import online.zhangwenzhe.blog.api.entity.BlogCommentExample;
import org.apache.ibatis.annotations.Param;

public interface BlogCommentMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    long countByExample(BlogCommentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int deleteByExample(BlogCommentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int insert(BlogComment record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int insertSelective(BlogComment record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    List<BlogComment> selectByExample(BlogCommentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    BlogComment selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByExampleSelective(@Param("record") BlogComment record, @Param("example") BlogCommentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByExample(@Param("record") BlogComment record, @Param("example") BlogCommentExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByPrimaryKeySelective(BlogComment record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_comment
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByPrimaryKey(BlogComment record);
}