package online.zhangwenzhe.blog.api.mapper;

import java.util.List;
import online.zhangwenzhe.blog.api.entity.BlogPost;
import online.zhangwenzhe.blog.api.entity.BlogPostExample;
import org.apache.ibatis.annotations.Param;

public interface BlogPostMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    long countByExample(BlogPostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int deleteByExample(BlogPostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int insert(BlogPost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int insertSelective(BlogPost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    List<BlogPost> selectByExampleWithBLOBs(BlogPostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    List<BlogPost> selectByExample(BlogPostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    BlogPost selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByExampleSelective(@Param("record") BlogPost record, @Param("example") BlogPostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByExampleWithBLOBs(@Param("record") BlogPost record, @Param("example") BlogPostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByExample(@Param("record") BlogPost record, @Param("example") BlogPostExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByPrimaryKeySelective(BlogPost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByPrimaryKeyWithBLOBs(BlogPost record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table blog_post
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    int updateByPrimaryKey(BlogPost record);
}