package online.zhangwenzhe.blog.api.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserAuthVo {
    private String username;

    private String cellphone;

    private String email;

    @NotNull(message = "{user.password.notBlank}")
    private String password;

    private String nickname;
}
