package online.zhangwenzhe.blog.api.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import java.util.Date;

@Data
public class UserExtVo {

    @Min(value = 1, message = "{user.name.notBlank}")
    private long id;

    @JsonProperty("is_deleted")
    private Byte isDeleted;

    private Date createdAt;

    private Date updatedAt;

    private Date deletedAt;

    private UserAuthVo userAuthVo;
}
