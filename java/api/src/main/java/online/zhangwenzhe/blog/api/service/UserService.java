package online.zhangwenzhe.blog.api.service;

import online.zhangwenzhe.blog.api.entity.BlogUserBasic;
import online.zhangwenzhe.blog.api.entity.BlogUserBasicExample;

import java.util.List;

public interface UserService {
    BlogUserBasic getByName(String name);

    List<BlogUserBasic> list(BlogUserBasicExample blogUserBasicExample);
}
