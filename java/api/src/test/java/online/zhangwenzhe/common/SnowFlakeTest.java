package online.zhangwenzhe.common;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SnowFlakeTest {
    @Test
    public void nextIdTest() {

        for (int i = 0; i < 100; i++) {
            System.out.println(SnowFlake.next());
        }
    }

    @Test
    public void startSTMP() {
        LocalDateTime localDateTime = LocalDateTime.of(2017, 7, 1, 0, 0, 0);
        System.out.println(localDateTime.toEpochSecond(ZoneOffset.of("+8")) * 1000L);
    }

    @Test
    public void performanceTest() {
        long avg = 0;
        for (int k = 0; k < 10; k++) {
            List<Callable<Long>> partitions = new ArrayList<Callable<Long>>();
            for (int i = 0; i < 1800000; i++) {
                partitions.add(new Callable<Long>() {
                    @Override
                    public Long call() throws Exception {
                        return SnowFlake.getInstance().nextId();
                    }
                });
            }
            ExecutorService executorPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            try {
                long s = System.currentTimeMillis();
                executorPool.invokeAll(partitions, 10000, TimeUnit.SECONDS);
                long s_avg = System.currentTimeMillis() - s;
                avg += s_avg;
                System.out.println("完成时间需要: " + s_avg / 1.0e3 + "秒");
                executorPool.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("平均完成时间需要: " + avg / 10 / 1.0e3 + "秒");
    }
}
