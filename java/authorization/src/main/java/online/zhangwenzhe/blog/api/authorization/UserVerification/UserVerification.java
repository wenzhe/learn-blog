package online.zhangwenzhe.blog.api.authorization.UserVerification;

import online.zhangwenzhe.blog.api.authorization.oauth2.User;

public interface UserVerification {
    /**
     * 验证用户
     * @param principal 用户标识，如用户名，手机号等
     * @param credentials 鉴权信息，如密码，验证码等
     * @return
     */
    User verifiy(String principal, String credentials);
}
