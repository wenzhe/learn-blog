package online.zhangwenzhe.blog.api.validator;

import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2019-01-13
 */
public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {
    @Override
    public void initialize(PhoneNumber constraintAnnotation) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (StringUtils.isEmpty(s)) {
            return false;
        }

        if (s.length() != 11) {
            return false;
        }

        if (!s.startsWith("1")) {
            return false;
        }
        return true;
    }
}
