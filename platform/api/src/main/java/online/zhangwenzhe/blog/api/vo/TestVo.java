package online.zhangwenzhe.blog.api.vo;

import lombok.Data;

import java.util.Date;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2019-03-07
 */
@Data
public class TestVo {
    private long id;
    private String tag;
    private Date date;
}
