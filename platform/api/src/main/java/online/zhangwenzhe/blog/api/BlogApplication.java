package online.zhangwenzhe.blog.api;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@MapperScan(basePackages = {"online.zhangwenzhe.blog.api.mapper"})
@ComponentScan(basePackages = {"online.zhangwenzhe.blog.api", "online.zhangwenzhe.common"})
@ImportResource(locations = {"classpath:ApplicationContext.xml"})
public class BlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
    }
}
