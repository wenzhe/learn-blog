package online.zhangwenzhe.blog.api.web;


import online.zhangwenzhe.blog.api.vo.TestVo;
import online.zhangwenzhe.common.resultwarper.JsonResult;
import online.zhangwenzhe.common.identity.NewID;
import online.zhangwenzhe.common.security.UserContext;
import online.zhangwenzhe.common.security.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Date;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public String get() {
        UserDetails userDetails = UserContext.getUserDetails();

        String msg = "/test/get is ok\n";
        if (userDetails != null) {
            msg += userDetails.getUserId();
        }
        return msg;
    }

    @RequestMapping(value = "/getJson", method = RequestMethod.GET)
    public JsonResult getJson() {
        return new JsonResult("hello\nworld");
    }

    @RequestMapping(value = "/getLong", method = RequestMethod.GET)
    public long getLong() {
        long ret = 999L;
        return ret;
    }

    @RequestMapping(value = "/getTestVo", method = RequestMethod.GET)
    public TestVo getTestVo() {
        TestVo testVo = new TestVo();
        testVo.setDate(new Date());
        testVo.setId(Long.MAX_VALUE);
        testVo.setTag("hello");
        return testVo;
    }

    @RequestMapping(value = "/postTestVo", method = RequestMethod.POST)
    public TestVo postTestVo(@RequestBody TestVo vo) {
        return vo;
    }

    @RequestMapping(value = "/getDate", method = RequestMethod.GET)
    public Date getDate(Date date) {
        return date;
    }

    @RequestMapping(value = "/getNewID", method = RequestMethod.GET)
    public long getNewID() {
        return NewID.next();
    }
}
