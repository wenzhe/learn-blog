package online.zhangwenzhe.blog.api.entity;

import java.util.Date;

public class AuthUserRole {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column auth_user_role.id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column auth_user_role.auth_user_id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    private Long authUserId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column auth_user_role.auth_role_id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    private Long authRoleId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column auth_user_role.is_deleted
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    private Byte isDeleted;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column auth_user_role.created_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    private Date createdAt;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column auth_user_role.updated_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    private Date updatedAt;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column auth_user_role.deleted_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    private Date deletedAt;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column auth_user_role.id
     *
     * @return the value of auth_user_role.id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column auth_user_role.id
     *
     * @param id the value for auth_user_role.id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column auth_user_role.auth_user_id
     *
     * @return the value of auth_user_role.auth_user_id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public Long getAuthUserId() {
        return authUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column auth_user_role.auth_user_id
     *
     * @param authUserId the value for auth_user_role.auth_user_id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public void setAuthUserId(Long authUserId) {
        this.authUserId = authUserId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column auth_user_role.auth_role_id
     *
     * @return the value of auth_user_role.auth_role_id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public Long getAuthRoleId() {
        return authRoleId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column auth_user_role.auth_role_id
     *
     * @param authRoleId the value for auth_user_role.auth_role_id
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public void setAuthRoleId(Long authRoleId) {
        this.authRoleId = authRoleId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column auth_user_role.is_deleted
     *
     * @return the value of auth_user_role.is_deleted
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public Byte getIsDeleted() {
        return isDeleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column auth_user_role.is_deleted
     *
     * @param isDeleted the value for auth_user_role.is_deleted
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column auth_user_role.created_at
     *
     * @return the value of auth_user_role.created_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column auth_user_role.created_at
     *
     * @param createdAt the value for auth_user_role.created_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column auth_user_role.updated_at
     *
     * @return the value of auth_user_role.updated_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public Date getUpdatedAt() {
        return updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column auth_user_role.updated_at
     *
     * @param updatedAt the value for auth_user_role.updated_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column auth_user_role.deleted_at
     *
     * @return the value of auth_user_role.deleted_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public Date getDeletedAt() {
        return deletedAt;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column auth_user_role.deleted_at
     *
     * @param deletedAt the value for auth_user_role.deleted_at
     *
     * @mbg.generated Fri Jun 29 16:28:15 CST 2018
     */
    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }
}