package online.zhangwenzhe.blog.api.web;

import online.zhangwenzhe.blog.api.entity.BlogUserBasic;
import online.zhangwenzhe.blog.api.service.UserServiceImpl;
import online.zhangwenzhe.blog.api.validator.PhoneNumber;
import online.zhangwenzhe.blog.api.vo.UserVo;
import online.zhangwenzhe.common.exception.BusinessException;
import online.zhangwenzhe.common.exception.BusinessExceptionResult;
import online.zhangwenzhe.common.security.AuthenticationContext;
import online.zhangwenzhe.common.security.UserContext;
import online.zhangwenzhe.common.security.TokenInformation;
import online.zhangwenzhe.common.security.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @RequestMapping(value = "/list")
    public List<BlogUserBasic> list() {

        return userService.list(null);
    }

    @RequestMapping(value = "/token")
    public UserDetails token() {
        UserDetails userDetails = UserContext.getUserDetails();
        return userDetails;
    }

    @RequestMapping(value = "/get")
    public BlogUserBasic get() {
        return new BlogUserBasic();
    }

    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public UserVo post(@RequestBody UserVo userVo,
                       @PhoneNumber String test) {
        return userVo;
    }

    @RequestMapping(path = "/update", method = RequestMethod.PUT)
    public String put() throws Exception {
        BlogUserBasic blogUserBasic = new BlogUserBasic();
        blogUserBasic.setId(1L);
        blogUserBasic.setNickname("ABC");

        userService.updateStart(blogUserBasic);
//        int a = 0;
//        int b = 1;
//        int c = b / a;
        return "post";
    }

    @RequestMapping(path = "/user", method = RequestMethod.DELETE)
    public String delete() {
        BusinessExceptionResult result = new BusinessExceptionResult("ASD001", "业务数据发生异常");
        throw new BusinessException(result);
    }
}
