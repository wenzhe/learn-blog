package online.zhangwenzhe.blog.old_common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import online.zhangwenzhe.blog.api.BlogApplicationTests;

import online.zhangwenzhe.common.security.datasource.AuthenticationService;
import online.zhangwenzhe.common.security.datasource.MenuService;
import online.zhangwenzhe.common.security.datasource.ResourceVo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AuthTest extends BlogApplicationTests {
    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    MenuService menuService;

    @Test
    public void loadResourceRoleMapTest() {
        List ret = authenticationService.getResourceRoleMap();
    }

    @Test
    public void getMenuTreeTest() throws JsonProcessingException {
        ResourceVo root = menuService.getMenuTree();
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(root);
    }

    @Test
    public void getAuthorizedMenuTest() throws JsonProcessingException {
        ResourceVo root = menuService.getAuthorizedMenu(1L);
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(root);
        System.out.println(json);
    }
}
