package online.zhangwenzhe.blog.old_common;

import online.zhangwenzhe.blog.api.BlogApplicationTests;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2018/8/14 下午7:02
 */

public class LogTest extends BlogApplicationTests {
    protected static final Logger logger = LoggerFactory.getLogger(LogTest.class);

    @Test
    public void run(){
        logger.info("123aaa");
        logger.debug("123aaa");
        logger.warn("123aaa");
        logger.error("123aaa");

    }
}
