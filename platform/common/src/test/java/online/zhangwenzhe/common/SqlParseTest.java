package online.zhangwenzhe.common;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLExpr;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.expr.SQLBinaryOpExpr;
import com.alibaba.druid.sql.ast.expr.SQLBinaryOperator;
import com.alibaba.druid.sql.ast.statement.*;
import com.alibaba.druid.sql.dialect.mysql.parser.MySqlStatementParser;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.alibaba.druid.sql.parser.SQLExprParser;
import com.alibaba.druid.sql.parser.SQLParserUtils;
import com.alibaba.druid.sql.parser.SQLStatementParser;
import com.alibaba.druid.util.JdbcConstants;
import com.alibaba.druid.util.JdbcUtils;
import online.zhangwenzhe.common.identity.SnowFlake;
import org.junit.Test;

import javax.websocket.Session;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2018/8/24 上午10:15
 */

public class SqlParseTest {

    //region sql

    String sql = "SELECT\n" +
            "\td.customerBaseId,\n" +
            "\td.isAddCommissionDetail,\n" +
            "\tpb.hmfBuildingId,\n" +
            "\td.id AS customerDetailId,\n" +
            "\tb.`name` AS userName,\n" +
            "\tb.mobile AS userMobile,\n" +
            "\tIFNULL(u.`name`, NULL) AS agentName,\n" +
            "\td.`status` AS STATUS,\n" +
            "\torg.`name` AS departName,\n" +
            "\torg_c.`name` AS companyName,\n" +
            "\td.agentUserId,\n" +
            "\t(\n" +
            "\t\tCASE\n" +
            "\t\tWHEN d.`status` = 'WAIT' THEN\n" +
            "\t\t\t'报备确认'\n" +
            "\t\tWHEN d.`status` = 'REPORTED' THEN\n" +
            "\t\t\t'到访确认'\n" +
            "\t\tWHEN d.`status` = 'VISITED' THEN\n" +
            "\t\t\t'成交确认'\n" +
            "\t\tWHEN d.`status` = 'DEAL' THEN\n" +
            "\t\t\t'已成交'\n" +
            "\t\tWHEN d.`status` = 'INVALIED' THEN\n" +
            "\t\t\t'无效报备'\n" +
            "\t\tEND\n" +
            "\t) AS customerStatus,\n" +
            "\tIFNULL(pb.`name`, NULL) AS buildingName\n" +
            "FROM\n" +
            "\tt_customer_detail d\n" +
            "LEFT JOIN t_customer_base b ON b.id = d.customerBaseId\n" +
            "LEFT JOIN t_agent_user u ON u.id = d.agentUserId\n" +
            "LEFT JOIN t_pro_building_base pb ON pb.id = d.buildingId\n" +
            "LEFT JOIN t_sys_org org ON org.id = u.orgId\n" +
            "LEFT JOIN t_sys_org org_c ON org_c.id = u.companyId\n" +
            "WHERE\n" +
            "\t1 = 1\n" +
            "AND d.buildingId IN (\n" +
            "\t3220,\n" +
            "\t3219,\n" +
            "\t1158,\n" +
            "\t1157,\n" +
            "\t1154,\n" +
            "\t1153,\n" +
            "\t1151,\n" +
            "\t1143,\n" +
            "\t1142,\n" +
            "\t1136,\n" +
            "\t1134,\n" +
            "\t1132,\n" +
            "\t1131,\n" +
            "\t1125,\n" +
            "\t1122,\n" +
            "\t1121,\n" +
            "\t1111,\n" +
            "\t1110,\n" +
            "\t1088,\n" +
            "\t1079,\n" +
            "\t1076,\n" +
            "\t1073,\n" +
            "\t1062,\n" +
            "\t979,\n" +
            "\t977,\n" +
            "\t975,\n" +
            "\t972,\n" +
            "\t962,\n" +
            "\t953,\n" +
            "\t952,\n" +
            "\t948,\n" +
            "\t946,\n" +
            "\t941,\n" +
            "\t921,\n" +
            "\t918,\n" +
            "\t905,\n" +
            "\t904,\n" +
            "\t876,\n" +
            "\t860,\n" +
            "\t855,\n" +
            "\t848,\n" +
            "\t838,\n" +
            "\t836,\n" +
            "\t835,\n" +
            "\t825,\n" +
            "\t822,\n" +
            "\t820,\n" +
            "\t818,\n" +
            "\t817,\n" +
            "\t811,\n" +
            "\t786,\n" +
            "\t773,\n" +
            "\t769,\n" +
            "\t768,\n" +
            "\t767,\n" +
            "\t766,\n" +
            "\t765,\n" +
            "\t764,\n" +
            "\t698,\n" +
            "\t697,\n" +
            "\t695,\n" +
            "\t635,\n" +
            "\t634,\n" +
            "\t612,\n" +
            "\t610,\n" +
            "\t608,\n" +
            "\t607,\n" +
            "\t606,\n" +
            "\t605,\n" +
            "\t602,\n" +
            "\t586,\n" +
            "\t563,\n" +
            "\t546,\n" +
            "\t543,\n" +
            "\t540,\n" +
            "\t536,\n" +
            "\t532,\n" +
            "\t524,\n" +
            "\t518,\n" +
            "\t517,\n" +
            "\t499,\n" +
            "\t498,\n" +
            "\t497,\n" +
            "\t431,\n" +
            "\t415,\n" +
            "\t390,\n" +
            "\t389,\n" +
            "\t386,\n" +
            "\t370,\n" +
            "\t369,\n" +
            "\t368,\n" +
            "\t366,\n" +
            "\t359,\n" +
            "\t350,\n" +
            "\t310,\n" +
            "\t302,\n" +
            "\t298,\n" +
            "\t270,\n" +
            "\t249,\n" +
            "\t228,\n" +
            "\t224,\n" +
            "\t210,\n" +
            "\t193,\n" +
            "\t165,\n" +
            "\t162,\n" +
            "\t151,\n" +
            "\t148,\n" +
            "\t147,\n" +
            "\t146,\n" +
            "\t144,\n" +
            "\t143,\n" +
            "\t142,\n" +
            "\t133,\n" +
            "\t131,\n" +
            "\t130,\n" +
            "\t129,\n" +
            "\t128,\n" +
            "\t127,\n" +
            "\t126,\n" +
            "\t125,\n" +
            "\t36,\n" +
            "\t20,\n" +
            "\t10,\n" +
            "\t19,\n" +
            "\t18,\n" +
            "\t22,\n" +
            "\t29,\n" +
            "\t11,\n" +
            "\t30,\n" +
            "\t28,\n" +
            "\t27,\n" +
            "\t26,\n" +
            "\t25,\n" +
            "\t24,\n" +
            "\t21,\n" +
            "\t17,\n" +
            "\t16,\n" +
            "\t15,\n" +
            "\t14,\n" +
            "\t13,\n" +
            "\t12,\n" +
            "\t9,\n" +
            "\t8\n" +
            ")\n" +
            "AND d.`status` IN (\n" +
            "\t'WAIT',\n" +
            "\t'REPORTED',\n" +
            "\t'VISITED',\n" +
            "\t'DEAL',\n" +
            "\t'INVALIED'\n" +
            ")\n" +
            "AND (\n" +
            "\tu.companyId IN (\n" +
            "\t\tSELECT\n" +
            "\t\t\tb.id\n" +
            "\t\tFROM\n" +
            "\t\t\tt_sys_org a,\n" +
            "\t\t\tt_sys_org b\n" +
            "\t\tWHERE\n" +
            "\t\t\ta.id = b.manageCompanyId\n" +
            "\t\tAND a.cityId = 86\n" +
            "\t\tAND a.parentId = 0\n" +
            "\t\tAND a.companyType = 'city'\n" +
            "\t\tAND a.valid = 1\n" +
            "\t\tAND b.companyType = 'company'\n" +
            "\t\tAND b.parentId = 0\n" +
            "\t)\n" +
            "\tOR u.companyId = 1\n" +
            ")\n" +
            "GROUP BY\n" +
            "\td.customerBaseId,\n" +
            "\td.agentUserId";

    String sql2 = "select *\n" +
            "from auth_user u\n" +
            "       join auth_user_role ur on u.id = ur.auth_user_id\n" +
            "       join (select id from auth_role) r on r.id = ur.auth_role_id\n" +
            "       join blog_user_basic b2 on u.id = b2.id\n" +
            "where r.id in (select id from auth_role where name like '张三%')";

    //endregion


    @Test
    public void dd() {
//        String dbType = JdbcConstants.MYSQL;
//
//        //格式化输出
//        String result = SQLUtils.format(sql, dbType);
////        System.out.println(result); // 缺省大写格式
//        List<SQLStatement> stmtList = SQLUtils.parseStatements(sql, dbType);
        SQLStatementParser parser = new MySqlStatementParser(sql2);
        List<SQLStatement> stmtList = parser.parseStatementList();


        SQLSelectStatement selectStmt = (SQLSelectStatement) stmtList.get(0);
        // 拿到SQLSelect 通过在这里打断点看对象我们可以看出这是一个树的结构
        SQLSelect sqlselect = selectStmt.getSelect();
        SQLSelectQueryBlock query = (SQLSelectQueryBlock) sqlselect.getQuery();
        SQLExpr whereExpr = query.getWhere();
        SQLTableSource fromExpr = query.getFrom();
//        // 修改where表达式
//        if (whereExpr == null) {
//            query.setWhere(constraintsExpr);
//        } else {
//            SQLBinaryOpExpr newWhereExpr = new SQLBinaryOpExpr(whereExpr, SQLBinaryOperator.BooleanAnd, constraintsExpr);
//            query.setWhere(newWhereExpr);
//        }
//        sqlselect.setQuery(query);
//        sql = sqlselect.toString();
    }

    private void fromExpr(SQLTableSource sqlTableSource) {
        // 边界条件
        if (sqlTableSource instanceof SQLExprTableSource) {

        }

        if (sqlTableSource instanceof SQLJoinTableSource) {
            SQLJoinTableSource sqlJoinTableSource = (SQLJoinTableSource) sqlTableSource;


        }
    }


    public List<Map<String, Object>> search(String sql, Map<String, Object> conditions) throws Exception {
        List<Map<String, Object>> result = new ArrayList<>();
        // SQLParserUtils.createSQLStatementParser可以将sql装载到Parser里面
        SQLStatementParser parser = SQLParserUtils.createSQLStatementParser(sql, JdbcUtils.MYSQL);
        // parseStatementList的返回值SQLStatement本身就是druid里面的语法树对象
        List<SQLStatement> stmtList = parser.parseStatementList();
        SQLStatement stmt = stmtList.get(0);
        if (stmt instanceof SQLSelectStatement) {
            // convert conditions to 'and' statement
            StringBuffer constraintsBuffer = new StringBuffer();
            Set<String> keys = conditions.keySet();
            Iterator<String> keyIter = keys.iterator();
            if (keyIter.hasNext()) {
                constraintsBuffer.append(keyIter.next()).append(" = ?");
            }
            while (keyIter.hasNext()) {
                constraintsBuffer.append(" AND ").append(keyIter.next()).append(" = ?");
            }
            SQLExprParser constraintsParser = SQLParserUtils.createExprParser(constraintsBuffer.toString(), JdbcUtils.MYSQL);
            SQLExpr constraintsExpr = constraintsParser.expr();
            SQLSelectStatement selectStmt = (SQLSelectStatement) stmt;
            // 拿到SQLSelect 通过在这里打断点看对象我们可以看出这是一个树的结构
            SQLSelect sqlselect = selectStmt.getSelect();
            SQLSelectQueryBlock query = (SQLSelectQueryBlock) sqlselect.getQuery();
            SQLExpr whereExpr = query.getWhere();
            // 修改where表达式
            if (whereExpr == null) {
                query.setWhere(constraintsExpr);
            } else {
                SQLBinaryOpExpr newWhereExpr = new SQLBinaryOpExpr(whereExpr, SQLBinaryOperator.BooleanAnd, constraintsExpr);
                query.setWhere(newWhereExpr);
            }
            sqlselect.setQuery(query);
            sql = sqlselect.toString();
//            Session session = sessionFactory.openSession();
//            SQLQuery sqlQuery = session.createSQLQuery(sql);
//            Collection values = conditions.values();
//            int index = 1;
//            for (Object value : values) {
//                sqlQuery.setParameter(index, value);
//                index++;
//            }
//            result = sqlQuery.list();
//            session.close();
        } else {
            throw new Exception("not select statement");
        }
        return result;
    }


//    @Test
    public void performanceTest() {
        long avg = 0;
        for (int k = 0; k < 10; k++) {
            List<Callable<Long>> partitions = new ArrayList<Callable<Long>>();
            for (int i = 0; i < 50000; i++) {
                partitions.add(new Callable<Long>() {
                    @Override
                    public Long call() throws Exception {
                        dd();
                        return 0L;
                    }
                });
            }
            ExecutorService executorPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            try {
                long s = System.currentTimeMillis();
                executorPool.invokeAll(partitions, 10000, TimeUnit.SECONDS);
                long s_avg = System.currentTimeMillis() - s;
                avg += s_avg;
                System.out.println("完成时间需要: " + s_avg / 1.0e3 + "秒");
                executorPool.shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("平均完成时间需要: " + avg / 10 / 1.0e3 + "秒");
    }
}
