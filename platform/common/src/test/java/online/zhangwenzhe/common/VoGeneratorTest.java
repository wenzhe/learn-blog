package online.zhangwenzhe.common;

import org.junit.Test;

import java.util.Date;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2019-03-07
 */
public class VoGeneratorTest {
    @Test
    public void run(){
        TestVo entity = new TestVo();
        entity.setId(123456L);
        entity.setTag("hello");
        entity.setDate(new Date());
        TestVo vo =VoGenerator.generate(entity,TestVo.class);
    }
}
