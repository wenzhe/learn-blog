package online.zhangwenzhe.common.security;

import org.springframework.stereotype.Component;

@Component
public interface Authenticator {
    /**
     * Token验证
     * @param token
     * @return 验证成功返回用户标识(TokenInformation),失败返回null
     */
    UserDetails verifyToken(String token);

    /**
     * 权限验证
     * @param userid        用户ID
     * @param uri
     * @param httpMethod
     * @return
     */
    boolean verifyAuthority(long userid, String uri, String httpMethod);

    /**
     * 资源是否允许匿名访问
     * @param uri
     * @param httpMethod
     * @return
     */
    boolean allowAnonymous(String uri, String httpMethod);
}
