package online.zhangwenzhe.common.security;


public interface AuthorityVerifier {
    /**
     * 权限验证
     * @param userid
     * @param uri
     * @param httpMethod
     * @return
     */
    boolean verify(long userid, String uri, String httpMethod);

    boolean allowAnonymous(String uri, String httpMethod);
}
