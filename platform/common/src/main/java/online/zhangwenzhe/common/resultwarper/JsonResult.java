package online.zhangwenzhe.common.resultwarper;


import java.io.Serializable;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2019-03-05
 */
public class JsonResult implements Serializable {
    Object value;

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public JsonResult(Object value) {
        this.value = value;
    }
}
