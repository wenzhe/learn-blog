package online.zhangwenzhe.common.security.Impl;

import online.zhangwenzhe.common.security.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class AuthenticatorImpl implements Authenticator {

    @Autowired
    TokenVerifier tokenVerifier;

    @Autowired
    AuthorityVerifier authorityVerifier;


    @Override
    public UserDetails verifyToken(String token) {
        if (StringUtils.isEmpty(token)) {
            return null;
        }
        UserDetails userDetails = tokenVerifier.verify(token);

        //todo: 需要缓存
        return userDetails;
    }

    @Override
    public boolean verifyAuthority(long userid, String uri, String httpMethod) {
        if (StringUtils.isEmpty(uri) || StringUtils.isEmpty(httpMethod)) {
            return false;
        }

        return authorityVerifier.verify(userid, uri, httpMethod);
    }

    @Override
    public boolean allowAnonymous(String uri, String httpMethod) {
        if (authorityVerifier.allowAnonymous(uri,httpMethod)){
            return true;
        }

        return false;
    }
}
