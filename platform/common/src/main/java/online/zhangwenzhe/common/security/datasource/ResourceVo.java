package online.zhangwenzhe.common.security.datasource;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class ResourceVo {

    private Long id;

    private String name;

    private String icon;

    private Boolean isShowInMenu;

    private String uri;

    private String method;

    private String description;

    private Long parentId;

    private Boolean hasPermission;

    @JsonIgnore
    private ResourceVo parent;

    private List<ResourceVo> children;

    private Boolean isDeleted;

    @Override
    public ResourceVo clone() {
        ResourceVo vo = new ResourceVo();
        vo.setId(this.id);
        vo.setName(this.name);
        vo.setIcon(this.icon);
        vo.setShowInMenu(this.isShowInMenu);
        vo.setUri(this.uri);
        vo.setMethod(this.method);
        vo.setDescription(this.description);
        vo.setParentId(this.parentId);
        return vo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getShowInMenu() {
        return isShowInMenu;
    }

    public void setShowInMenu(Boolean showInMenu) {
        isShowInMenu = showInMenu;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Boolean getHasPermission() {
        return hasPermission;
    }

    public void setHasPermission(Boolean hasPermission) {
        this.hasPermission = hasPermission;
    }

    public ResourceVo getParent() {
        return parent;
    }

    public void setParent(ResourceVo parent) {
        this.parent = parent;
    }

    public List<ResourceVo> getChildren() {
        return children;
    }

    public void setChildren(List<ResourceVo> children) {
        this.children = children;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }
}