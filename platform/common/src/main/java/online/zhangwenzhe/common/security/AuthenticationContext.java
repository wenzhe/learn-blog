package online.zhangwenzhe.common.security;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2019-02-21
 */
@Deprecated(since = "更新方法用UserContext中的getUserDetails获取当前用户信息")
public class AuthenticationContext {
    TokenInformation token;


    /**
     * @return          当前用户ID
     * @deprecated      更新方法用UserContext中的getUserDetails获取当前用户信息
     */
    public static String getUserId(){
        HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        Object attr = request.getAttribute("user");
        if (attr !=null){
            return attr.toString();
        }

        return null;
    }
}
