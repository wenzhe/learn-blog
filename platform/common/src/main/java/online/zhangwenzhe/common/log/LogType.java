package online.zhangwenzhe.common.log;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2018/8/15 下午4:01
 */

public enum LogType {

    /*
    HTTP请求记录
     */
    HTTP_TRACE(9, "HTTP请求记录"),

    /*
    HTTP请求记录
     */
    EXCEPTION(1, "异常日志");

    @JsonValue
    private int code;

    private String description;

    LogType(int code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String toString() {
        return Integer.toString(this.code);
    }
}
