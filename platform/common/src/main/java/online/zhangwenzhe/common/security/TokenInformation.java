package online.zhangwenzhe.common.security;

import java.io.Serializable;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2019-02-22
 */
@Deprecated(since = "更新使用UserDetails")
public class TokenInformation implements Serializable {
    String exp;
    String user_name;
    String[] authorities;
    String client_id;
    String[] scope;

    public TokenInformation() {
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String[] getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String[] authorities) {
        this.authorities = authorities;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String[] getScope() {
        return scope;
    }

    public void setScope(String[] scope) {
        this.scope = scope;
    }
}
