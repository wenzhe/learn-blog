package online.zhangwenzhe.common.security.datasource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthenticationService {

    AuthenticationResourceDao authenticationResourceDao;

    @Autowired
    public AuthenticationService(AuthenticationResourceDao authenticationResourceDao) {
        this.authenticationResourceDao = authenticationResourceDao;
    }

    public List<String> getRoleByUser(Long userId) {
        return authenticationResourceDao.getRoleByUser(userId);
    }

    public List<ResourceRoleMapVo> getResourceRoleMap() {
        return authenticationResourceDao.loadResourceRoleMap();
    }

    public List<ResourceVo> getGrantedResource(long userId){
        return authenticationResourceDao.getGrantedResource(userId);
    }

    public  List<ResourceVo> getAllResource(){
        return authenticationResourceDao.getAllResource();
    }
}
