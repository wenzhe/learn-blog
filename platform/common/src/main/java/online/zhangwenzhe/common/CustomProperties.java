package online.zhangwenzhe.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * Description
 * 自定义配置信息
 *
 * @author zhangwenzhe
 * @since 2018/8/15 下午3:23
 */

@Component
@ConfigurationProperties(prefix = "soda")
@PropertySource(value = "classpath:/custom.properties", ignoreResourceNotFound = true)
public class CustomProperties {
    private OAuth2 oauth2;

    private Application application;

    public OAuth2 getOauth2() {
        return oauth2;
    }

    public void setOauth2(OAuth2 oauth2) {
        this.oauth2 = oauth2;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public static class OAuth2 {

        @NotNull
        private boolean enable;

        @NotNull
        private boolean allowAnonymous;

        @NotNull
        private String tokenEndpoint;

        @NotNull
        private String clientId;

        @NotNull
        private String clientSecret;

        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }

        public String getTokenEndpoint() {
            return tokenEndpoint;
        }

        public void setTokenEndpoint(String tokenEndpoint) {
            this.tokenEndpoint = tokenEndpoint;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public void setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
        }

        public boolean isAllowAnonymous() {
            return allowAnonymous;
        }

        public void setAllowAnonymous(boolean allowAnonymous) {
            this.allowAnonymous = allowAnonymous;
        }
    }

    public static class Application {
        private String name = "myApplication";
        private int datacenterId = 1;
        private int machineId = 1;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getDatacenterId() {
            return datacenterId;
        }

        public void setDatacenterId(int datacenterId) {
            this.datacenterId = datacenterId;
        }

        public int getMachineId() {
            return machineId;
        }

        public void setMachineId(int machineId) {
            this.machineId = machineId;
        }
    }
}
