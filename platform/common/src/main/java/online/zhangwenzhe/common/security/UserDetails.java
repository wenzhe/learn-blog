package online.zhangwenzhe.common.security;

import lombok.Data;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2019-03-04
 */
@Data
public class UserDetails {
    long userId;
    String client;

    @Deprecated(since = "功能没有开发完，不提供使用")
    String username;
    @Deprecated
    String phone;
    @Deprecated
    String email;
    @Deprecated
    String wechatMiniAppOpenId;

}
