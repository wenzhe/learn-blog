//package online.zhangwenzhe.common.dataauthority;
//
//import net.sf.jsqlparser.parser.CCJSqlParserManager;
//import net.sf.jsqlparser.parser.CCJSqlParserUtil;
//import net.sf.jsqlparser.statement.Statement;
//import net.sf.jsqlparser.statement.select.Select;
//import org.apache.ibatis.executor.Executor;
//import org.apache.ibatis.executor.statement.RoutingStatementHandler;
//import org.apache.ibatis.executor.statement.StatementHandler;
//import org.apache.ibatis.mapping.*;
//import org.apache.ibatis.plugin.*;
//import org.apache.ibatis.reflection.MetaObject;
//import org.apache.ibatis.reflection.SystemMetaObject;
//import org.apache.ibatis.session.ResultHandler;
//import org.apache.ibatis.session.RowBounds;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.util.HashMap;
//import java.util.Properties;
//
///**
// * Description
// *
// * @author zhangwenzhe
// * @since 2018/8/22 下午3:05
// */
//
////@Intercepts({
////        @Signature(method = "query", type = Executor.class, args = {
////                MappedStatement.class, Object.class, RowBounds.class,
////                ResultHandler.class}),
////        @Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
//
//@Intercepts({
//        @Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})
//})
//@Component
//public class PrepareInterceptor implements Interceptor {
//    /**
//     * 日志
//     */
//    private static final Logger log = LoggerFactory.getLogger(PrepareInterceptor.class);
//
//    @Override
//    public Object plugin(Object target) {
//        return Plugin.wrap(target, this);
//    }
//
//    @Override
//    public void setProperties(Properties properties) {
//    }
//
//    @Override
//    public Object intercept(Invocation invocation) throws Throwable {
//        if(invocation.getTarget() instanceof RoutingStatementHandler) {
//
//            RoutingStatementHandler handler = (RoutingStatementHandler) invocation.getTarget();
//            BoundSql boundSql = handler.getBoundSql();
//        }
//
//
////        StatementHandler handler = (StatementHandler)invocation.getTarget();
////        //由于mappedStatement中有我们需要的方法id,但却是protected的，所以要通过反射获取
////        MetaObject statementHandler = SystemMetaObject.forObject(handler);
////        MappedStatement mappedStatement = (MappedStatement) statementHandler.getValue("delegate.mappedStatement");
////        //获取sql
////        BoundSql boundSql = handler.getBoundSql();
////        String sql = boundSql.getSql();
////        //获取方法id
////        String id = mappedStatement.getId();
////        if ("需要增强的方法的id".equals(id)) {
////            //增强sql代码块
////        }
//        return invocation.proceed();
//
//    }
//}
