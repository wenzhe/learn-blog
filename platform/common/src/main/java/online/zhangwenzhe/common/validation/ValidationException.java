package online.zhangwenzhe.common.validation;

public class ValidationException extends RuntimeException {

    ValidationResult validationResult;

    public ValidationResult getValidationResult() {
        return validationResult;
    }

    public ValidationException(ValidationResult validationResult) {
        this.validationResult = validationResult;
    }

    public ValidationException(String message, ValidationResult validationResult) {
        super(message);
        this.validationResult = validationResult;
    }
}
