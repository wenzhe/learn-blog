package online.zhangwenzhe.common;


import org.springframework.beans.BeanUtils;

/**
 * Description
 * 对象拷贝工具，方便生成Vo
 * @author zhangwenzhe
 * @since 2019-03-07
 */
public class VoGenerator {
    public static <T> T generate(Object entity, Class<T> type) {
        try {
            T vo = type.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(entity, vo);
            return vo;
        } catch (Exception e) {
            return null;
        }

    }
}
