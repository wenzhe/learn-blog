import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * Description
 *
 * @author zhangwenzhe
 * @since 2018/11/16 3:00 PM
 */

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    public static void run(String[] args) throws IOException {
        System.out.println("测试启动");
    }
}