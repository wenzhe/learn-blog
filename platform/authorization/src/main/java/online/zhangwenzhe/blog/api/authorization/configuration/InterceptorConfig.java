package online.zhangwenzhe.blog.api.authorization.configuration;

import online.zhangwenzhe.common.security.AuthenticationInterceptor;
import online.zhangwenzhe.common.token.TokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// WebMvcConfigurerAdapter，用下面代码替换
//@Configuration
//public class InterceptorConfig extends WebMvcConfigurerAdapter {
//
//    @Autowired
//    private TokenInterceptor tokenInterceptor;
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
////        registry.addInterceptor(tokenInterceptor)
////                .addPathPatterns("/**");
//    }
//}

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    private TokenInterceptor tokenInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(tokenInterceptor)
//                .addPathPatterns("/**");
    }
}