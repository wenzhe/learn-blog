package online.zhangwenzhe.blog.api.authorization.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;

@Service
public class UserService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    String sql = "select * from auth_user where username =? limit 0,1";

    public User getByName(String username) {

        SqlRowSet rowSet = jdbcTemplate.queryForRowSet(sql, new Object[]{username});

        User user = null;
        if (rowSet.next()) {
            Collection<SimpleGrantedAuthority> authorities = new HashSet<SimpleGrantedAuthority>();
//            authorities.add(new SimpleGrantedAuthority("/user/list"));
//            authorities.add(new SimpleGrantedAuthority("/**"));
            authorities.add(new SimpleGrantedAuthority("ROLE_ANONYMOUS"));
            user = new User(rowSet.getLong("id"), rowSet.getString("username"), rowSet.getString("password"), authorities);
        }

        return user;
    }
}
