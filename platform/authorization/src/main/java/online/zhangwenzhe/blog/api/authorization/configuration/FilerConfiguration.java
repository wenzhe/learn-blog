package online.zhangwenzhe.blog.api.authorization.configuration;

import online.zhangwenzhe.common.trace.TracingFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilerConfiguration {
    @Bean
    public FilterRegistrationBean tracingFilterRegistration() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new TracingFilter());
        registration.addUrlPatterns("/*");
        registration.addInitParameter("paramName", "paramValue");
        registration.setName("TracingFilter");
        registration.setOrder(Integer.MIN_VALUE);
        return registration;
    }
}
