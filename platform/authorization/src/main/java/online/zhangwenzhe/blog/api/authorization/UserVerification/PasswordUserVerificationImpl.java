package online.zhangwenzhe.blog.api.authorization.UserVerification;

import online.zhangwenzhe.blog.api.authorization.oauth2.User;
import online.zhangwenzhe.blog.api.authorization.oauth2.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Qualifier("PasswordUserVerificationImpl")
@Component
public class PasswordUserVerificationImpl implements UserVerification {
    @Autowired
    private UserService userService;

    @Override
    public User verifiy(String principal, String credentials) {
        User user = userService.getByName(principal);

        if (user == null) {
            throw new UsernameNotFoundException("用户名：" + principal + "不存在！");
        }

        if (!credentials.equals(user.getPassword())) {
            throw new BadCredentialsException("用户和密码不匹配");
        }

        return user;
    }
}
