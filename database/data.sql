

INSERT INTO zhangwenzhe_blog.auth_resource (id, name, icon, is_show_in_menu, uri, method, description, parent_id, is_deleted, created_at, updated_at, deleted_at) VALUES (1, '根目录', null, 1, '/**', 'GET', '根目录', null , 0, '2018-05-16 15:05:27', '2018-05-18 17:13:34', null);
INSERT INTO zhangwenzhe_blog.auth_resource (id, name, icon, is_show_in_menu, uri, method, description, parent_id, is_deleted, created_at, updated_at, deleted_at) VALUES (2, '用户管理', null, 1, '/user/**', 'GET', 'USER', 1, 0, '2018-05-16 15:06:53', '2018-05-18 17:13:35', null);
INSERT INTO zhangwenzhe_blog.auth_resource (id, name, icon, is_show_in_menu, uri, method, description, parent_id, is_deleted, created_at, updated_at, deleted_at) VALUES (3, '用户列表', null, 1, '/user/list', 'GET', '', 2, 0, '2018-05-16 15:06:53', '2018-05-16 20:44:12', null);
INSERT INTO zhangwenzhe_blog.auth_resource (id, name, icon, is_show_in_menu, uri, method, description, parent_id, is_deleted, created_at, updated_at, deleted_at) VALUES (4, '新建用户', null, 1, '/user/create', 'POST', '', 2, 0, '2018-05-16 15:31:39', '2018-05-16 20:44:12', null);
INSERT INTO zhangwenzhe_blog.auth_resource (id, name, icon, is_show_in_menu, uri, method, description, parent_id, is_deleted, created_at, updated_at, deleted_at) VALUES (5, '分类管理', null, 1, '/category/**', 'GET', '', 1, 0, '2018-05-22 09:33:09', null, null);
INSERT INTO zhangwenzhe_blog.auth_resource (id, name, icon, is_show_in_menu, uri, method, description, parent_id, is_deleted, created_at, updated_at, deleted_at) VALUES (6, '分类列表', null, 1, '/category/List', 'GET', '', 5, 0, '2018-05-23 11:13:50', '2018-05-24 10:44:45', null);
INSERT INTO zhangwenzhe_blog.auth_resource (id, name, icon, is_show_in_menu, uri, method, description, parent_id, is_deleted, created_at, updated_at, deleted_at) VALUES (7, '新增分类', null, 1, '/category/create', 'POST', '', 5, 0, '2018-05-23 11:13:50', '2018-05-24 10:44:45', null);
INSERT INTO zhangwenzhe_blog.auth_resource (id, name, icon, is_show_in_menu, uri, method, description, parent_id, is_deleted, created_at, updated_at, deleted_at) VALUES (8, '编辑分类', null, 1, '/category/update', 'PUT', '', 5, 0, '2018-05-23 11:13:50', '2018-05-24 10:44:45', null);

INSERT INTO zhangwenzhe_blog.auth_role (id, name, description, is_deleted, created_at, updated_at, deleted_at) VALUES (1, '管理员', '全部权限', 0, '2018-05-16 15:35:42', '2018-05-18 17:03:39', null);
INSERT INTO zhangwenzhe_blog.auth_role (id, name, description, is_deleted, created_at, updated_at, deleted_at) VALUES (2, '编辑', '', 0, '2018-05-16 20:47:50', '2018-05-18 17:03:39', null);


INSERT INTO zhangwenzhe_blog.auth_permission (id, auth_resource_id, auth_role_id, is_deleted, created_at, updated_at, deleted_at) VALUES (1, 3, 1, 0, '2018-05-16 20:45:07', '2018-05-24 11:09:21', null);
INSERT INTO zhangwenzhe_blog.auth_permission (id, auth_resource_id, auth_role_id, is_deleted, created_at, updated_at, deleted_at) VALUES (2, 4, 2, 0, '2018-05-16 20:48:31', '2018-05-24 10:42:07', null);
INSERT INTO zhangwenzhe_blog.auth_permission (id, auth_resource_id, auth_role_id, is_deleted, created_at, updated_at, deleted_at) VALUES (3, 6, 2, 0, '2018-05-18 13:45:11', '2018-05-24 10:41:48', null);
INSERT INTO zhangwenzhe_blog.auth_permission (id, auth_resource_id, auth_role_id, is_deleted, created_at, updated_at, deleted_at) VALUES (4, 8, 1, 0, '2018-05-18 15:43:22', '2018-05-24 11:08:31', null);

INSERT INTO zhangwenzhe_blog.auth_user (id, username, cellphone, email, password, is_deleted, created_at, updated_at, deleted_at) VALUES (1, '张文哲', '18512371700', 'zwz911@sina.com', '123456', 0, '2018-05-24 17:20:17', null, null);

INSERT INTO zhangwenzhe_blog.auth_user_role (id, auth_user_id, auth_role_id, is_deleted, created_at, updated_at, deleted_at) VALUES (1, 1, 1, 0, '2018-05-24 17:20:39', null, null);


