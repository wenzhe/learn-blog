-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema zhangwenzhe_blog
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `auth_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `auth_user` ;

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` BIGINT UNSIGNED NOT NULL,
  `username` VARCHAR(45) NOT NULL DEFAULT '',
  `cellphone` VARCHAR(45) NOT NULL DEFAULT '',
  `email` VARCHAR(45) NOT NULL DEFAULT '',
  `password` VARCHAR(45) NOT NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog_user_basic`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_user_basic` ;

CREATE TABLE IF NOT EXISTS `blog_user_basic` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nickname` VARCHAR(45) NOT NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_user_basic_auth_user1`
  FOREIGN KEY (`id`)
  REFERENCES `auth_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_category` ;

CREATE TABLE IF NOT EXISTS `blog_category` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `description` VARCHAR(1024) NULL,
  `category_id` BIGINT UNSIGNED NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_category_category1_idx` (`category_id` ASC),
  CONSTRAINT `fk_category_category1`
  FOREIGN KEY (`category_id`)
  REFERENCES `blog_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog_post`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_post` ;

CREATE TABLE IF NOT EXISTS `blog_post` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  `category_id` BIGINT UNSIGNED NOT NULL,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_post_category1_idx` (`category_id` ASC),
  INDEX `fk_post_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_post_category1`
  FOREIGN KEY (`category_id`)
  REFERENCES `blog_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_user1`
  FOREIGN KEY (`user_id`)
  REFERENCES `blog_user_basic` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_tag` ;

CREATE TABLE IF NOT EXISTS `blog_tag` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `keyword` VARCHAR(255) NOT NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog_comment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_comment` ;

CREATE TABLE IF NOT EXISTS `blog_comment` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` VARCHAR(1024) NOT NULL,
  `post_id` BIGINT UNSIGNED NOT NULL,
  `user_id` BIGINT UNSIGNED NOT NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_comment_post1_idx` (`post_id` ASC),
  INDEX `fk_comment_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_comment_post1`
  FOREIGN KEY (`post_id`)
  REFERENCES `blog_post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_user1`
  FOREIGN KEY (`user_id`)
  REFERENCES `blog_user_basic` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog_attachment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_attachment` ;

CREATE TABLE IF NOT EXISTS `blog_attachment` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` SMALLINT NOT NULL,
  `url` VARCHAR(1024) NOT NULL,
  `post_id` BIGINT UNSIGNED NOT NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_attachment_post_idx` (`post_id` ASC),
  CONSTRAINT `fk_attachment_post`
  FOREIGN KEY (`post_id`)
  REFERENCES `blog_post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog_post_has_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_post_has_tag` ;

CREATE TABLE IF NOT EXISTS `blog_post_has_tag` (
  `id` BIGINT UNSIGNED NOT NULL,
  `post_id` BIGINT UNSIGNED NOT NULL,
  `tag_id` BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_post_has_tag_tag1_idx` (`tag_id` ASC),
  INDEX `fk_post_has_tag_post1_idx` (`post_id` ASC),
  CONSTRAINT `fk_post_has_tag_post1`
  FOREIGN KEY (`post_id`)
  REFERENCES `blog_post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_has_tag_tag1`
  FOREIGN KEY (`tag_id`)
  REFERENCES `blog_tag` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `blog_user_extend`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `blog_user_extend` ;

CREATE TABLE IF NOT EXISTS `blog_user_extend` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` BLOB NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_user_extend_user1`
  FOREIGN KEY (`id`)
  REFERENCES `blog_user_basic` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auth_resource`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `auth_resource` ;

CREATE TABLE IF NOT EXISTS `auth_resource` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(45) NULL,
  `icon` VARCHAR(45) NULL,
  `is_show_in_menu` TINYINT UNSIGNED NOT NULL DEFAULT 1 COMMENT '�Ƿ��ڲ˵�����ʾ\n1.��ʾ\n2.����',
  `order` SMALLINT NOT NULL DEFAULT 0,
  `uri` NVARCHAR(255) NOT NULL DEFAULT '',
  `method` VARCHAR(45) NOT NULL DEFAULT '',
  `description` NVARCHAR(255) NOT NULL DEFAULT '',
  `parent_id` BIGINT UNSIGNED NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_auth_resource_auth_resource1_idx` (`parent_id` ASC),
  CONSTRAINT `fk_auth_resource_auth_resource1`
  FOREIGN KEY (`parent_id`)
  REFERENCES `auth_resource` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auth_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `auth_role` ;

CREATE TABLE IF NOT EXISTS `auth_role` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(45) NULL,
  `description` NVARCHAR(255) NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auth_permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `auth_permission` ;

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_resource_id` BIGINT UNSIGNED NOT NULL,
  `auth_role_id` BIGINT UNSIGNED NOT NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_auth_permission_auth_resource1_idx` (`auth_resource_id` ASC),
  INDEX `fk_auth_permission_auth_role1_idx` (`auth_role_id` ASC),
  CONSTRAINT `fk_auth_permission_auth_resource1`
  FOREIGN KEY (`auth_resource_id`)
  REFERENCES `auth_resource` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_auth_permission_auth_role1`
  FOREIGN KEY (`auth_role_id`)
  REFERENCES `auth_role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `auth_user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `auth_user_role` ;

CREATE TABLE IF NOT EXISTS `auth_user_role` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `auth_user_id` BIGINT UNSIGNED NOT NULL,
  `auth_role_id` BIGINT UNSIGNED NOT NULL,
  `is_deleted` TINYINT UNSIGNED NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_auth_user_role_auth_role1_idx` (`auth_role_id` ASC),
  INDEX `fk_auth_user_role_auth_user1_idx` (`auth_user_id` ASC),
  CONSTRAINT `fk_auth_user_role_auth_role1`
  FOREIGN KEY (`auth_role_id`)
  REFERENCES `auth_role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_auth_user_role_auth_user1`
  FOREIGN KEY (`auth_user_id`)
  REFERENCES `auth_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
